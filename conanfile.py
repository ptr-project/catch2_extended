# Copyright (c) 2020 Kai Hoewelmeyer
#
# Permission is hereby granted, free of charge, to any person obtaining a copy of
# this software and associated documentation files (the "Software"), to deal in
# the Software without restriction, including without limitation the rights to
# use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies
# of the Software, and to permit persons to whom the Software is furnished to do
# so, subject to the following conditions:
#
# The above copyright notice and this permission notice shall be included in all
# copies or substantial portions of the Software.
#
# THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
# IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
# FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
# AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
# LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
# OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
# SOFTWARE.

from conans import tools, ConanFile, CMake
from conans.errors import ConanInvalidConfiguration
import os.path


class Catch2ExtendedConan(ConanFile):
    name = "catch2_extended"
    license = "<Put the package license here>"
    author = "<Put your name here> <And your email here>"
    url = "<Package recipe repository url here, for issues about the package>"
    description = "<Description of catch2_extended here>"
    topics = ("<Put some tag here>", "<here>", "<and here>")
    settings = "os", "compiler", "build_type", "arch"
    options = {"shared": [True, False],
               "fPIC": [True, False],
               "build_testing": [True, False]}
    default_options = {"shared": False,
                       "build_testing": False,
                       "fPIC": True,
                       "boost:header_only": True}
    generators = "cmake"
    requires = ["boost/1.74.0", "catch2/2.13.6"]
    build_requires = [
        "cmake_utils/0.4@ptr-project+conan-packages/stable"]
    exports_sources = ["cmake/*", "CMakeLists.txt", "src/*"]

    def configure(self):
        if self.options.build_testing:
            self.options["boost"].header_only = False

    def build(self):
        cmake = CMake(self)
        cmake.configure(defs={"BUILD_TESTING": self.options.build_testing})
        cmake.build()
        if self.options.build_testing:
            cmake.test()

    def package(self):
        self.copy("*.h", dst="include", src="src/include")
        self.copy("*.lib", dst="lib", keep_path=False)
        self.copy("*.dll", dst="bin", keep_path=False)
        self.copy("*.dylib*", dst="lib", keep_path=False)
        self.copy("*.so", dst="lib", keep_path=False)
        self.copy("*.a", dst="lib", keep_path=False)

    def package_info(self):
        self.cpp_info.libs = ["catch2_extended"]
        self.cpp_info.defines = ["CATCH_CONFIG_ENABLE_BENCHMARKING"]

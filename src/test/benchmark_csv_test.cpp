/**
 * Copyright (c) 2020 Kai Hoewelmeyer
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */

#include <boost/dll/runtime_symbol_info.hpp>
#include <boost/process.hpp>

#include <catch2/catch.hpp>
#include <catch2_extended/custom_macros.h>

#include <string>

namespace {
std::string RunBenchmark(const std::string& testCase) {
  namespace bp = boost::process;

  bp::ipstream is;

  const auto returnCode = bp::system(
      bp::exe = boost::dll::program_location(),
      bp::args = {testCase, "-r", "benchmark-csv", "--benchmark-samples", "1"},
      bp::std_out > is);
  CHECK(returnCode == 0);

  std::string output;
  std::string line;
  while (std::getline(is, line)) {
    output += line + "\n";
  }

  return output;
}
}  // namespace

namespace catch2_extended {

using Catch::Matchers::Contains;
using Catch::Matchers::Matches;

TEST_CASE("NoStatistics_Benchmark", "[!hide]") {
  int i = 0;
  BENCHMARK("MyBenchmark1") { return ++i; };
}

TEST_CASE("NoStatistics") {
  const auto output = RunBenchmark("NoStatistics_Benchmark");
  CHECK_THAT(output, Contains("MyBenchmark1"));
  CHECK_THAT(
      output,
      Matches("name,sample\\(ns\\),outlier_variance,"
              "statistics\n\"MyBenchmark1\",[0-9.e+-]+,[0-9.e+-na]+,\"\"\n"));
}

TEST_CASE("Statistics_Benchmark", "[!hide]") {
  int i = 0;
  CAPTURE(i);
  BENCHMARK_STATISTICS("MyBenchmark2")(auto meter) {
    meter.measure([&] { return ++i; });
    meter["i"] = "test";
    meter["int"] = 5;
    meter["float"] = 0.42;
  };
}

TEST_CASE("CaptureAndStatistics") {
  const auto output = RunBenchmark("Statistics_Benchmark");  // NOLINT
  CHECK_THAT(output, Contains("MyBenchmark2"));
  CHECK_THAT(output, Contains("i := \"\"test\"\""));
  CHECK_THAT(output, Contains("int := 5"));
  CHECK_THAT(output, Contains("float := 0.42"));
}

}  // namespace catch2_extended

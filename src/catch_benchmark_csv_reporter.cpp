/**
 * Copyright (c) 2020 Kai Hoewelmeyer
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */

#define CATCH_CONFIG_EXTERNAL_INTERFACES
#include "catch2_extended/benchmark_csv_reporter.h"
#include "catch2_extended/custom_statistics.h"

#include <boost/algorithm/string/join.hpp>
#include <boost/algorithm/string/replace.hpp>

#include <catch2/catch.hpp>

#include <algorithm>
#include <cassert>
#include <chrono>
#include <cstdint>
#include <iterator>
#include <memory>
#include <string>
#include <unordered_map>
#include <vector>

namespace {
template <class... Ts>
struct overloaded : Ts... {  // NOLINT(readability-identifier-naming)
  using Ts::operator()...;
};

template <class... Ts>
overloaded(Ts...) -> overloaded<Ts...>;
}  // namespace

namespace catch2_extended {

// NOLINTNEXTLINE(readability-identifier-naming)
std::string BenchmarkCsvReporter::getDescription() {
  return "Reports csv style results for benchmarks";
}

void BenchmarkCsvReporter::BeginRun() {
  if (!benchmarkRunning_) {
    return;
  }
  statistics_.emplace_back();
}

void BenchmarkCsvReporter::CaptureStatistic(const std::string &key,
                                            const std::string &value) {
  if (!benchmarkRunning_) {
    return;
  }
  assert(!statistics_.empty());
  statistics_.back()[key] = value;
}

void BenchmarkCsvReporter::testRunStarting(
    const Catch::TestRunInfo &testRunInfo) {
  return StreamingReporterBase::testRunStarting(testRunInfo);
}

void BenchmarkCsvReporter::testRunEnded(const Catch::TestRunStats &stats) {
  return StreamingReporterBase::testRunEnded(stats);
}

void BenchmarkCsvReporter::benchmarkStarting(
    const Catch::BenchmarkInfo & /*info*/) {
  benchmarkRunning_ = true;
}

namespace {
template <typename TContainer>
[[nodiscard]] static std::string EscapeAll(const TContainer &container) {
  std::vector<std::string> escaped;
  escaped.reserve(container.size());
  std::transform(container.begin(), container.end(),
                 std::back_inserter(escaped), [](const auto &v) -> std::string {
                   overloaded getString{
                       [](const Catch::MessageInfo &info) {
                         std::string capture = info.message;

                         return capture;
                       },
                       [](const std::pair<std::string, std::string> &pair) {
                         return pair.first + " := " + pair.second;
                       }};
                   // Replace contained double-quotes with double-double
                   // quotes as per RFC4180
                   return boost::replace_all_copy(getString(v), "\"", "\"\"");
                 });

  return boost::join(escaped, ";");
}
}  // namespace

void BenchmarkCsvReporter::benchmarkEnded(
    Catch::BenchmarkStats<> const &stats) {
  benchmarkRunning_ = false;

  StreamingReporterBase::benchmarkEnded(stats);
  PrintHeaderIfNecessary();

  assert(statistics_.empty() || statistics_.size() == stats.samples.size());

  const auto &samples = stats.samples;
  for (auto i = 0u; i < stats.samples.size(); ++i) {
    stream << "\"" << stats.info.name << "\"," << samples[i].count() << ","
           << stats.outlierVariance << ",\"";
    if (!statistics_.empty()) {
      stream << EscapeAll(statistics_[i]);
    }
    stream << "\"" << std::endl;
  }

  statistics_.clear();
}

void BenchmarkCsvReporter::benchmarkFailed(std::string const &error) {
  StreamingReporterBase::stream << "Benchmark failed with " << error
                                << std::endl;
}

// IStreamingReporter interface
void BenchmarkCsvReporter::assertionStarting(
    const Catch::AssertionInfo & /*assertionInfo*/) {}
bool BenchmarkCsvReporter::assertionEnded(
    const Catch::AssertionStats & /*assertionStats*/) {
  return true;
}

void BenchmarkCsvReporter::PrintHeaderIfNecessary() {
  if (headerIsPrinted_) {
    return;
  }

  stream << "name,sample(ns),outlier_variance,statistics" << std::endl;

  headerIsPrinted_ = true;
}

std::vector<std::unordered_map<std::string, std::string>>
    BenchmarkCsvReporter::statistics_;
bool BenchmarkCsvReporter::benchmarkRunning_ = false;

void CaptureStatistics(const std::string &key, const std::string &val) {
  BenchmarkCsvReporter::CaptureStatistic(key, val);
}

void BeginRun() { BenchmarkCsvReporter::BeginRun(); }

}  // namespace catch2_extended

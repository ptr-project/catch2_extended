/**
 * Copyright (c) 2020 Kai Hoewelmeyer
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */

#define CATCH_CONFIG_EXTERNAL_INTERFACES

#include <catch2/catch.hpp>

#include <string>
#include <unordered_map>
#include <vector>

namespace catch2_extended {

struct BenchmarkCsvReporter
    : public Catch::StreamingReporterBase<BenchmarkCsvReporter> {
  explicit BenchmarkCsvReporter(const Catch::ReporterConfig &config)
      : StreamingReporterBase(config) {}

  // NOLINTNEXTLINE(readability-identifier-naming)
  static std::string getDescription();

  /**
   * @brief Prepare new run (single execution of code under benchmark
   */
  static void BeginRun();

  static void CaptureStatistic(const std::string &key,
                               const std::string &value);

  void testRunStarting(const Catch::TestRunInfo &testRunInfo) override;

  void testRunEnded(const Catch::TestRunStats &stats) override;

  void benchmarkStarting(const Catch::BenchmarkInfo & /*info*/) override;

  void benchmarkEnded(Catch::BenchmarkStats<> const &stats) override;

  void benchmarkFailed(std::string const &error) override;

  // IStreamingReporter interface
  void assertionStarting(
      const Catch::AssertionInfo & /*assertionInfo*/) override;
  bool assertionEnded(
      const Catch::AssertionStats & /*assertionStats*/) override;

 private:
  void PrintHeaderIfNecessary();

  bool headerIsPrinted_{false};
  static std::vector<std::unordered_map<std::string, std::string>> statistics_;
  static bool benchmarkRunning_;
};

// NOLINTNEXTLINE
CATCH_REGISTER_REPORTER("benchmark-csv", BenchmarkCsvReporter)
}  // namespace catch2_extended

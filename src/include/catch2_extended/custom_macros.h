/**
 * Copyright (c) 2020 Kai Hoewelmeyer
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */

#pragma once

#include <catch2/catch.hpp>
#include <catch2_extended/custom_statistics.h>
#include <catch2_extended/outlier_variance_listener.h>

#include <iostream>
#include <string>

#ifdef CATCH_CONFIG_ENABLE_BENCHMARKING
#include <catch2_extended/benchmark_csv_reporter.h>

// Keep consistency with Catch2 macros
// NOLINTNEXTLINE(cppcoreguidelines-macro-usage)
#define BENCHMARK_ADVANCED_STABLE(name)                                 \
  catch2_extended::OutlierVarianceListener::Reset();                    \
  while (!catch2_extended::OutlierVarianceListener::CheckRunVariance()) \
  BENCHMARK_ADVANCED(name)

namespace catch2_extended {

namespace detail {
template <bool Stable>
struct BenchmarkInvoker;

template <>
struct BenchmarkInvoker<false> {
  template <typename Fun>
  void operator()(std::string&& name, Fun&& fun) {
    BENCHMARK_ADVANCED(std::move(name))(auto rawMeter) {
      catch2_extended::StatisticsMeter meter(rawMeter);
      fun(meter);
    };
  }
};

template <>
struct BenchmarkInvoker<true> {
  template <typename Fun>
  void operator()(const std::string& name, Fun&& fun) {
    BENCHMARK_ADVANCED_STABLE(std::string(name))(auto rawMeter) {
      catch2_extended::StatisticsMeter meter(rawMeter);
      fun(meter);
    };
  }
};
}  // namespace detail

template <bool Stable>
class StatBenchmark {
 public:
  explicit StatBenchmark(std::string&& name) : name_{std::move(name)} {}

  explicit operator bool() const { return true; }

  template <typename Fun>
  StatBenchmark& operator=(Fun&& func) {
    detail::BenchmarkInvoker<Stable>{}(std::move(name_),
                                       std::forward<Fun>(func));
    return *this;
  }

 private:
  std::string name_;
};

}  // namespace catch2_extended

// Keep consistency with Catch2 macros
// NOLINTNEXTLINE(cppcoreguidelines-macro-usage)
#define BENCHMARK_STATISTICS(name) \
  if (catch2_extended::StatBenchmark<false> b{name}) b = [&]

// Keep consistency with Catch2 macros
// NOLINTNEXTLINE(cppcoreguidelines-macro-usage)
#define BENCHMARK_STABLE_STATISTICS(name) \
  if (catch2_extended::StatBenchmark<true> b{name}) b = [&]

#endif

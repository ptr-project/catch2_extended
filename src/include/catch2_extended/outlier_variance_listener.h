/**
 * Copyright (c) 2020 Kai Hoewelmeyer
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */

#define CATCH_CONFIG_EXTERNAL_INTERFACES
#include <catch2/catch.hpp>

#include <ostream>
#include <string>

#ifdef CATCH_CONFIG_ENABLE_BENCHMARKING

namespace catch2_extended {
struct OutlierVarianceListener : public Catch::IStreamingReporter {
  explicit OutlierVarianceListener(const Catch::ReporterConfig& config)
      : stream_{config.stream()} {}

  void benchmarkEnded(const Catch::BenchmarkStats<>& stats) override {
    lastOutlierVariance_ = stats.outlierVariance;
  }

  static void Reset();

  /**
   * @return True iff no further runs are necessary
   */
  static bool CheckRunVariance();

  // Boilerplate for IStreamingReporter
  [[nodiscard]] Catch::ReporterPreferences getPreferences() const override {
    return {};
  }
  void noMatchingTestCases(const std::string& /*spec*/) override {}
  void testRunStarting(const Catch::TestRunInfo& /*testRunInfo*/) override {}
  void testGroupStarting(const Catch::GroupInfo& /*groupInfo*/) override {}
  void testCaseStarting(const Catch::TestCaseInfo& /*testInfo*/) override {}
  void sectionStarting(const Catch::SectionInfo& /*sectionInfo*/) override {}
  void assertionStarting(
      const Catch::AssertionInfo& /*assertionInfo*/) override {}
  bool assertionEnded(
      const Catch::AssertionStats& /*assertionStats*/) override {
    return true;
  }
  void sectionEnded(const Catch::SectionStats& /*sectionStats*/) override {}
  void testCaseEnded(const Catch::TestCaseStats& /*testCaseStats*/) override {}
  void testGroupEnded(
      const Catch::TestGroupStats& /*testGroupStats*/) override {}
  void testRunEnded(const Catch::TestRunStats& /*testRunStats*/) override {}
  void skipTest(const Catch::TestCaseInfo& /*testInfo*/) override {}

 private:
  std::ostream& stream_;

  static double lastOutlierVariance_;
  static double minOutlierVariance_;
  static int runs_;
  static int runsSinceLastImprovement_;
};
}  // namespace catch2_extended

#endif  // CATCH_CONFIG_ENABLE_BENCHMARKING

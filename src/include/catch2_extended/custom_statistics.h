/**
 * Copyright (c) 2020 Kai Hoewelmeyer
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */

#pragma once

#include <string>
#include <utility>

namespace catch2_extended {

void CaptureStatistics(const std::string& key, const std::string& value);
void BeginRun();

class StatisticProxy {
 public:
  explicit StatisticProxy(std::string key) : key_{std::move(key)} {}

  StatisticProxy& operator=(const std::string& value) {
    CaptureString(value);
    return *this;
  }

  StatisticProxy& operator=(const char* const value) {
    CaptureString(value);
    return *this;
  }

  template <typename T>
  StatisticProxy& operator=(const T& value) {
    CaptureStatistics(key_, std::to_string(value));
    return *this;
  }

 private:
  void CaptureString(const std::string& value) {
    CaptureStatistics(key_, '"' + value + '"');
  }

  std::string key_;
};

template <typename TMeter>
class StatisticsMeter {
 public:
  explicit StatisticsMeter(TMeter& meter) : meter_{meter} { BeginRun(); }

  template <typename Fun>
  // NOLINTNEXTLINE(readability-identifier-naming)
  void measure(Fun&& fun) {
    meter_.measure(std::forward<Fun>(fun));
  }

  StatisticProxy operator[](const std::string& key) {
    return StatisticProxy{key};
  }

 private:
  TMeter& meter_;
};

}  // namespace catch2_extended

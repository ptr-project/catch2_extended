/**
 * Copyright (c) 2020 Kai Hoewelmeyer
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */

#include "catch2_extended/outlier_variance_listener.h"

#include <iostream>

#ifdef CATCH_CONFIG_ENABLE_BENCHMARKING

namespace catch2_extended {
void OutlierVarianceListener::Reset() {
  lastOutlierVariance_ = 1.0;
  minOutlierVariance_ = 1.0;
  runs_ = 0;
  runsSinceLastImprovement_ = 0;
}

bool OutlierVarianceListener::CheckRunVariance() {
  runs_++;

  if (lastOutlierVariance_ < minOutlierVariance_) {
    minOutlierVariance_ = lastOutlierVariance_;
    std::cerr << "Run has lowest outlier variance so far: "
              << minOutlierVariance_ << std::endl;
    runsSinceLastImprovement_ = 0;
  } else {
    runsSinceLastImprovement_++;
  }

  if (runs_ >= 100) {
    Catch::cerr()
        << "Giving up after a total of 100 runs, best outlier variance: "
        << minOutlierVariance_ << std::endl;
    return true;
  }

  if (runsSinceLastImprovement_ >= 10) {
    Catch::cerr() << "No improvements in 10 runs, giving up with best outlier "
                     "variance: "
                  << minOutlierVariance_ << std::endl;
    return true;
  }

  if (minOutlierVariance_ <= 0.01) {
    Catch::cerr()
        << "Run has outlier variance of at most 1%, success with best "
           "outlier variance: "
        << minOutlierVariance_ << std::endl;
    return true;
  }

  return false;
}

double OutlierVarianceListener::lastOutlierVariance_ = 1.0;
double OutlierVarianceListener::minOutlierVariance_ = 1.0;
int OutlierVarianceListener::runs_ = 0;
int OutlierVarianceListener::runsSinceLastImprovement_ = 0;

CATCH_REGISTER_LISTENER(OutlierVarianceListener)
}  // namespace catch2_extended

#endif  // CATCH_CONFIG_ENABLE_BENCHMARKING
